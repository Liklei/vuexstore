import Vue from 'vue'
import Vuex from 'vuex'
import dialogStore from './dialog_store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dialog: dialogStore
  }
})
